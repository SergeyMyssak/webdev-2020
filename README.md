## About me

**ФИ**: Мысак Сергей

**Группа**: ВТиП-402

___

**Роль**: архитектор, кодер 

**Языки программирования**: JavaScript, TypeScript

**Технологии**: React, React Native (Android & iOS)

___

**Intuit**
1. "Web-технологии" [certificate](https://www.intuit.ru/verifydiplomas/101304599)
2. "Операционная система Linux" [certificate](https://intuit.ru/verifydiplomas/101378104)
3. "Введение в Django" [certificate](https://intuit.ru/verifydiplomas/101378143)
4. "Web-программирование на PHP 5.2" [certificate](https://intuit.ru/verifydiplomas/101378129)

**Coursera**  
1. "Front-End Web UI Frameworks and Tools: Bootstrap 4" [certificate](https://coursera.org/share/5961a8b2f2036ac9d26bc7245a2a3777)
2. "Front-End Web Development with React" [certificate](https://coursera.org/share/c7ad7954a985d9eef1c7b74260284fe0)
3. "Multiplatform Mobile App Development with React Native" [certificate](https://coursera.org/share/a0977ca002440fb4ddd4be9fa156e714)
4. "Server-side Development with NodeJS, Express and MongoDB" [certificate](https://coursera.org/share/7bc78ce882c3bbf2484619d33210f7d3)
