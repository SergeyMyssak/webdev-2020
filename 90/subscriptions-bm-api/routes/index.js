const express = require('express');
const iap = require('in-app-purchase');
const { google } = require('googleapis');
const { JWT } = require('google-auth-library');

const config = require('../secrets/config');
const secrets = require('../secrets/secrets');

const router = express.Router();

google.options({
  auth: new JWT(
      secrets.client_email,
      null,
      secrets.private_key,
      ['https://www.googleapis.com/auth/androidpublisher'],
  )
});

const androidGoogleApi = google.androidpublisher({ version: 'v3' });
const androidPackageName = config.ANDROID_PACKAGE_NAME;

iap.config({
  appleExcludeOldTransactions: true,
  applePassword: secrets.apple_password,

  googleServiceAccount: {
    clientEmail: secrets.client_email,
    privateKey: secrets.private_key,
  },

  test: config.TEST_MODE,
});

router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.post('/', async function(req, res) {
  const { appType, purchase } = req.body;

  try {
    const receipt = appType === 'ios' ? purchase.transactionReceipt : {
      packageName: androidPackageName,
      productId: purchase.productId,
      purchaseToken: purchase.purchaseToken,
      subscription: true,
    };

    await processPurchase(appType, receipt);
    res.send('true');
  } catch (e) {
    res.send('false');
  }
});

async function processPurchase(app, receipt) {
  console.log('PROCESS PURCHASE - START');

  try {
    await iap.setup();
    console.log('iap.setup - success');
    const validationResponse = await iap.validate(receipt);
    console.log('validationResponse', validationResponse);

    const purchaseData = iap.getPurchaseData(validationResponse);
    const firstPurchaseItem = purchaseData[0];

    const isCancelled = iap.isCanceled(firstPurchaseItem);
    const isExpired = iap.isExpired(firstPurchaseItem);
    const { productId } = firstPurchaseItem;
    const origTxId = app === 'ios' ? firstPurchaseItem.originalTransactionId : firstPurchaseItem.transactionId;
    const latestReceipt = app === 'ios' ? validationResponse.latest_receipt : receipt;
    const startDate = app === 'ios' ? new Date(firstPurchaseItem.originalPurchaseDateMs) : new Date(parseInt(firstPurchaseItem.startTimeMillis, 10));
    const endDate = app === 'ios' ? new Date(firstPurchaseItem.expiresDateMs) : new Date(parseInt(firstPurchaseItem.expiryTimeMillis, 10));

    let environment = '';
    if (app === 'ios') {
      environment = validationResponse.sandbox ? 'sandbox' : 'production';
    }

    const values = {
      isCancelled,
      isExpired,
      origTxId,
      latestReceipt,
      startDate,
      endDate,
      environment
    };

    for (const key in values) {
      console.log(key, values[key]);
    }

    if (app === 'android' && validationResponse.acknowledgementState === 0) {
      await androidGoogleApi.purchases.subscriptions.acknowledge({
        packageName: androidPackageName,
        subscriptionId: productId,
        token: receipt.purchaseToken,
      });
    }

    console.log('PROCESS PURCHASE - END');
  } catch (e) {
    console.log('e', e);
  }
}

module.exports = router;
