import * as React from 'react';
import { BecomeDeveloperScreen } from './screens';

const App = (): JSX.Element => <BecomeDeveloperScreen />;

export default App;
