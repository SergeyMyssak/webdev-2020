import React, { memo } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';

const ActivityIndicatorSplash = (): JSX.Element => {
  return <ActivityIndicator style={styles.container} />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default memo(ActivityIndicatorSplash);
