import React, { FC, memo } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ISubscription } from '@types';
import { Subscription } from './components';

interface IProps {
  subscriptions: ISubscription[];
  onPressSubscription: (id: string) => void;
  onPressPurchase: () => void;
  onPressRestore: () => void;
}

const PurchasesView: FC<IProps> = ({
  subscriptions,
  onPressSubscription,
  onPressPurchase,
  onPressRestore,
}): JSX.Element => {
  const subLen = subscriptions.length;

  const renderSubscription = (item, index): JSX.Element => (
    <Subscription
      key={item.data.productId}
      subscription={item}
      isLast={index === subLen}
      onPressSubscription={onPressSubscription}
    />
  );

  const renderPurchaseBtn = (): JSX.Element => (
    <TouchableOpacity style={styles.purchaseBtn} onPress={onPressPurchase}>
      <Text style={styles.purchaseBtnText}>Purchase</Text>
    </TouchableOpacity>
  );

  const renderRestorePurchase = (): JSX.Element => (
    <TouchableOpacity
      style={styles.restorePurchaseBtn}
      onPress={onPressRestore}
    >
      <Text style={styles.restorePurchaseBtnText}>Restore purchase</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      {subscriptions.map(renderSubscription)}
      {renderPurchaseBtn()}
      {renderRestorePurchase()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingHorizontal: 24,
  },
  purchaseBtn: {
    paddingVertical: 16,
    paddingHorizontal: 24,
    backgroundColor: '#0D5F93',
    borderRadius: 8,
    marginTop: 36,
  },
  purchaseBtnText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
  restorePurchaseBtn: {
    paddingVertical: 16,
    paddingHorizontal: 24,
  },
  restorePurchaseBtnText: {
    color: '#0D5F93',
    fontSize: 14,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    marginTop: 32,
  },
});

export default memo(PurchasesView);
