import React, { FC, memo } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { IPriceForPeriod } from '@types';

interface IProps {
  data: IPriceForPeriod;
}

const SubscriptionBadge: FC<IProps> = ({ data }): JSX.Element => {
  const { currency, price, type } = data;

  return (
    <View style={styles.container}>
      <Text style={styles.price}>
        {currency} {price} /
      </Text>
      <Text style={styles.period}> {type}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'rgba(13, 95, 147, 0.2)',
  },
  period: {
    color: 'rgba(0,0,0,0.8)',
    fontSize: 10,
  },
  price: {
    color: 'rgba(0,0,0,0.8)',
    fontSize: 10,
    fontWeight: 'bold',
  },
});

export default memo(SubscriptionBadge);
