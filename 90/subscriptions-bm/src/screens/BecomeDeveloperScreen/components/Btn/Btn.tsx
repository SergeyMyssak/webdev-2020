import React, { FC, memo } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

interface IProps {
  text: string;
  onPress: () => void;
}

const Btn: FC<IProps> = ({ text, onPress }): JSX.Element => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={0.5}
    style={styles.container}
  >
    <Text style={styles.text}>{text}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    height: 48,
    minWidth: '100%',
    backgroundColor: '#0088cc',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 8,
  },
  text: {
    fontSize: 14,
    color: 'white',
    alignSelf: 'center',
  },
});

export default memo(Btn);
